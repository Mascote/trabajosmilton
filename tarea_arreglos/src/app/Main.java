package app;

import java.util.Scanner;
import java.util.Vector;

public class Main {
	int n , m;
	
	Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args){
		Main m = new Main();
		System.out.println("Teclee el numero del problema que quiere testear (ej. 1)");
		
		

		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		switch (a) {
		case 1:
			System.out.println("1.Hacer  un m�todo que genere una matriz n x m  \n "
					+ " en la cual asigne ceros a todos los elementos, \n"
					+ " excepto a los de la diagonal principal donde asignaran unos.");
			System.out.println();
			m.problema1();
			break;
		case 2:
			
			System.out.println("2.Codificar un programa que genere una matriz n x m \n"
					+ "con ceros en la diagonal principal hacia arriba.");
			System.out.println();
			m.problema2();
			break;
		case 3:
			System.out.println("3.Dada una matriz de M*M elementos, hacer un programa que \n"
					+ "construya un vector B, donde cada uno de sus componentes sea la suma de \n"
					+ "los elementos de valores num�ricos pares de las filas de la matriz.");
			System.out.println();
			m.problema3();
			break;
		case 4:
			System.out.println("4.Escribir un programa que lea las dimensiones de una matriz, \n"
					+ "lea y visualice la matriz y a continuaci�n encuentre el mayor y menor \n"
					+ "elemento de la matriz y sus posiciones.");
			System.out.println();
			m.problema4();
			break;

		default:
			break;
		}
	  
	}
	
	public void problema1(){
		
		  
		    System.out.print("intoduzca numero de filas del areglo (entero): ");
		    n = scanner.nextInt();
		    System.out.print("intoduzca numero de columnas del areglo (entero): ");
		     m = scanner.nextInt();
		    
		    int[][] arr = new int[n][m];
		    for (int i = 0; i < n; i++) {
				for (int j = 0; j < m ;j++) {
					//arr[i][j] = scanner.nextInt();
					if(j==i)
						System.out.print("1 ");
					else
						System.out.print(arr[i][j]+" ");
				}
				System.out.println();
			}
		  
	}
	public void problema2(){
		  
		    System.out.print("intoduzca numero de filas del areglo (entero): ");
		    n = scanner.nextInt();
		    System.out.print("intoduzca numero de columnas del areglo (entero): ");
		     m = scanner.nextInt();
		     boolean flag = false;
		    int[][] arr = new int[n][m];
		    for (int i = 0; i < n; i++) {
				for (int j = 0; j < m ;j++) {
					//arr[i][j] = scanner.nextInt();
					if(j==i)
						flag = true;
					if(flag)
						System.out.print("1 ");
					else
						System.out.print(arr[i][j]+" ");
				}
				System.out.println();
				flag = false;
			}
	}
	public void problema3(){
	
		    System.out.print("intoduzca largo del areglo (entero): ");
		    n = scanner.nextInt();
		    Vector<Integer> vec = new Vector<>(n);
		   for (int i = 0; i <n; i++) {
			   vec.add(i,0);
		}
		   System.out.print("intoduzca los valores del arreglo separados por espacio (enteros): ");
		    for (int i = 0; i < n; i++) {
				for (int j = 0; j < n ;j++) {
					int aux = scanner.nextInt();
					if (aux%2 == 0) 
						vec.set(j, vec.get(j)+aux );
				}
				
			}
		    for (int in : vec) {
				System.out.print(in+"  ");
			}
	
	}
	public void problema4(){
		int may, men , xmay=0, ymay=0,xmen=0,ymen=0;
		
		
		    System.out.print("intoduzca largo del areglo (entero): ");
		    n = scanner.nextInt();
		   
		    int [][] arr = new int [n][n];
		    System.out.print("intoduzca los valores del arreglo separados por espacio (enteros): ");
			   
		    for (int i = 0; i < n; i++){ 
				for (int j = 0; j < n ;j++) {
					int aux = scanner.nextInt();
					arr [i][j] = aux;
				}
		    }
				
		    
			
			may = arr[0][0];

			men = arr[0][0];

			
		    for (int i = 0; i < n; i++) {
				for (int j = 0; j < n ;j++) {
				if (arr[i][j] > may){
					may = arr[i][j];
					xmay = i;
					ymay = j;
				}
				if (arr[i][j] < men){
					men = arr[i][j];
					xmen = i;
					ymen = j;
				}
				
				}
				
			}
		    System.out.println("el menor es "+men+" y= "+xmen+" x= "+ymen);

		    System.out.println("el mayor es "+may+" y= "+xmay+" x= "+ymay);
			
	}

}
